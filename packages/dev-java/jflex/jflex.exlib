# Copyright 2009-2014 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require ant java

export_exlib_phases src_prepare src_install

SUMMARY="Fast scanner generator for Java"
DESCRIPTION="
JFlex is a lexical analyzer generator (also known as scanner generator) for Java,
written in Java. JFlex is designed to work together with the LALR parser generator
CUP by Scott Hudson, and the Java modification of Berkeley Yacc BYacc/J by Bob
Jamison. It can also be used together with other parser generators like ANTLR or
as a standalone tool.
"
HOMEPAGE="https://${PN}.de/"
DOWNLOADS="
    https://${PN}.de/${PNV}.tar.gz
    mirror://sourceforge/junit/junit-4.5.jar
"

REMOTE_IDS="freecode:${PN} sourceforge:${PN}"

UPSTREAM_DOCUMENTATION="https://${PN}.de/manual.html [[ lang = en ]]"
UPSTREAM_CHANGELOG="https://${PN}.de/history.html [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-java/java_cup[>=0.11a]
"

ANT_SRC_COMPILE_PARAMS=( jar )

WORK="${WORK}/src"

jflex_src_prepare() {
    edo mkdir "${WORK}"/../tools
    local cup_ver=$(best_version dev-java/java_cup)
    cup_ver=${cup_ver%%:*}
    cup_ver=${cup_ver/*\/}
    cup_ver=${cup_ver/0.}

    edo ln -s /usr/share/java_cup/${cup_ver/_/-}.jar "${WORK}"/../tools/java_cup.jar
    edo cp "${WORK}"/../lib/JFlex.jar "${WORK}"/../tools/
    edo cp "${FETCHEDDIR}"/junit-4.5.jar "${WORK}"/../tools/junit.jar
}

jflex_src_install() {
    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    doins "${WORK}"/../lib/JFlex.jar
}

