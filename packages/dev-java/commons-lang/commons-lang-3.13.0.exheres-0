# Copyright 2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require java [ virtual_jdk_and_jre_deps=true ]

SUMMARY="Apache Commons Lang"
DESCRIPTION="
A package of Java utility classes for the classes that are in java.lang's
hierarchy, or are considered to be so standard as to justify existence in
java.lang.
"
HOMEPAGE="https://commons.apache.org/proper/commons-lang"
DOWNLOADS="mirror://apache/commons/lang/source/${PN}$(ever major)-${PV}-src.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

WORK=${WORKBASE}/${PN}$(ever major)-${PV}-src

JAVA_SRC_DIR="src/main/java/org/apache/commons"

src_compile() {
     local sources=sources.lst
     local classes=target/classes

     edo find ${JAVA_SRC_DIR:-*} -name \*.java > ${sources}
     edo mkdir -p ${classes}
     edo javac -verbose -d ${classes} -encoding ISO-8859-1 @${sources}
     edo jar cfv ${PN}.jar -C ${classes} .
}

src_install() {
    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    newins ${PN}.jar ${PN}$(ever major)-${PV}.jar
}

