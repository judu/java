# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Provides alternatives handling for JRE exhereses. If you import this exlib DO
# NOT import java-jdk.exlib.

require alternatives

# Set use_versioned_jre_home according to where the package's main JRE tree
# (i.e. its JRE_HOME as some Java projects call it) is installed to. JREs
# importing this exlib should typically install the JRE tree to
# /usr/lib*/${PN}-${SLOT} and should therefore set
# use_versioned_jre_home=${SLOT}. Likewise, if the tree is installed to
# /usr/lib*/${PNV} then set use_versioned_jre_home=${PV}.
#
# If the JRE is installed to an unversioned directory (/usr/lib*/${PN}) then
# don't set use_versioned_jre_home.
myexparam use_versioned_jre_home=
exparam -v use_versioned_jre_home use_versioned_jre_home

_get_jre_home() {
    illegal_in_global_scope

    echo "/usr/$(exhost --target)/lib/${PN}${use_versioned_jre_home:+-${use_versioned_jre_home}}"
}

# do_jre_alternatives
#
# Set up java-jre alternatives automatically. Call this only from src_install,
# preferably at the end, or at least after all binaries and man pages have been
# installed to ${IMAGE}. Installed binaries are expected to be in
# ${IMAGE}/${jre_home}/bin -- DO NOT put standard JRE binaries in
# ${IMAGE}/usr/bin because alternatives will handle symlinking there for you.
# Likewise, installed man pages are expected to be in
# ${IMAGE}/${jre_home}/man/man1, NOT ${IMAGE}/usr/share/man/man1.
#
# do_jre_alternatives is intended to work with JREs that follow the standard
# Sun JRE tree. DO NOT call do_jre_alternatives for JREs with non-standard
# directory trees or non-standard naming for binaries and man pages (GCJ is one
# such deviant). For those you will need to provide custom alternatives
# mappings and possibly custom symlink trees to mimic a standard JRE layout
# (see gcj-jdk for an example of both).
do_jre_alternatives() {
    illegal_in_global_scope
    [[ ${EXHERES_PHASE} == install ]] || die "${FUNCNAME} can only be called from src_install"

    # Be careful; this will shadow a ${jre_home} in the caller's scope
    local jre_home="$(_get_jre_home)"

    local alternatives_map_bin=( ) \
          alternatives_map_man=( )

    for binary in "${IMAGE}${jre_home}"/bin/* ; do
        if [[ ! -d "${binary}" && -x "${binary}" ]] ; then
            binary="${binary##*/}"
            alternatives_map_bin+=(
                "/usr/$(exhost --target)/bin/${binary}" "${jre_home}/bin/${binary}"
            )
        fi
    done

    for manpage in "${IMAGE}${jre_home}"/man/man1/* ; do
        [[ ! "${manpage}" =~ .+\.1$ ]] && continue
        manpage="${manpage##*/}"
        alternatives_map_man+=(
            "/usr/share/man/man1/${manpage}" "${jre_home}/man/man1/${manpage}"
        )
    done

    alternatives_for "java-jre" "${PN}-${SLOT}" "${SLOT}" \
        "/usr/$(exhost --target)/lib/jre" "${jre_home}" \
        "${alternatives_map_bin[@]}" \
        "${alternatives_map_man[@]}"
}

